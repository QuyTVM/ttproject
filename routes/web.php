<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//trang chu
Route::get('/home',[HomeController::class,'home'])->name('home');

//login, register
Route::get('/login',[AuthController::class,'showLogin']);
Route::get('/register',[AuthController::class,'showRegister']);
Route::post('/register',[AuthController::class,'register'])->name('register');
Route::post('/login',[AuthController::class,'handleLogin'])->name('login');
Route::get('/logout',[AuthController::class,'logout'])->name('logout');

// chi-tiet-sp
Route::get('/products/{product}',[HomeController::class,'product'])->name('product');

//cart
Route::get('/cart',[CartController::class,'index'])->name('cart');
Route::post('/cart/add/{product}',[CartController::class,'addCart'])->name('cart.add');
Route::post('/cart/update/{product}',[CartController::class,'updateToCart'])->name('cart.update');

//checkout
Route::get('/checkout',[CartController::class,'showCheck'])->name('check')->middleware(['auth','cartNotEmpty']);
Route::post('/order',[CartController::class,'handleCheck'])->name('order')->middleware(['auth','cartNotEmpty']);

//filter
Route::get('/filter/{cate}',[FilterController::class, 'index'])->name('filter');

//comment
Route::post('/add-comment/{product}',[CommentController::class,'store'])->name('comment.add')->middleware('auth');

//admin
Route::prefix('admin')->middleware('checkAdmin')->group(function(){
    Route::get('/home',[AdminController::class,'showAdmin'])->name('admin.home');

    Route::prefix('user')->group(function(){
        Route::get('/home',[AdminController::class,'showUser'])->name('admin.user.home');
        Route::get('/add',[AdminController::class,'showCreateUser'])->name('admin.user.showcreate');
        Route::get('/edit/{user}',[AdminController::class,'showEditUser'])->name('admin.user.showedit');

        Route::post('/create',[AdminController::class,'handleAddUser'])->name('admin.user.add');
        Route::put('/edit/{user}',[AdminController::class,'handleEditUser'])->name('admin.user.edit');
        Route::delete('/delete/{user}',[AdminController::class,'destroy'])->name('admin.user.del');

    });

    Route::prefix('product')->group(function(){
        Route::get('/home',[ProductController::class,'showProduct'])->name('admin.product.home');
        Route::get('/add',[ProductController::class,'showCreateProduct'])->name('admin.product.showcreate');
        Route::get('/edit/{product}',[ProductController::class,'showEditProduct'])->name('admin.product.showedit');

        Route::post('/add',[ProductController::class,'handleAddProduct'])->name('admin.product.add');
        Route::put('/edit/{product}',[ProductController::class,'handleEditProduct'])->name('admin.product.edit');
        Route::delete('/delete/{product}',[ProductController::class,'destroy'])->name('admin.product.del');
    });

    Route::prefix('category')->group(function(){
        Route::get('/home',[CategoryController::class,'show'])->name('admin.category.home');
        Route::get('/add',[CategoryController::class,'create'])->name('admin.category.create');
        Route::get('/edit/{category}',[CategoryController::class,'edit'])->name('admin.category.edit');

        Route::post('/add',[CategoryController::class,'insert'])->name('admin.category.add');
        Route::put('/edit/{category}',[CategoryController::class,'update'])->name('admin.category.update');
        Route::delete('/delete/{category}',[CategoryController::class,'destroy'])->name('admin.category.del');
    });

    Route::prefix('order')->group(function(){
        Route::get('/home',[OrderController::class,'show'])->name('admin.order.home');
        Route::put('/up/{order}',[OrderController::class,'updateStatus'])->name('admin.order.status');
        Route::get('/detail/{order}',[OrderController::class,'showOrderDetail'])->name('admin.order.detail');
    });

    Route::prefix('comment')->group(function(){
        Route::get('/home',[OrderController::class,'showComment'])->name('admin.comment.home');
        Route::put('/status/{comment}',[OrderController::class,'changeStatusComment'])->name('admin.comment.status');
        Route::delete('/delete/{comment}',[OrderController::class,'deleteComment'])->name('admin.comment.del');
    });
});

//profile
Route::prefix('profile')->middleware('auth')->group(function(){
    Route::get('/profile',[ProfileController::class,'index'])->name('profile');
    Route::get('/form-up',[ProfileController::class,'showformupdate'])->name('updateuser');
    Route::get('/hisorder',[ProfileController::class,'showorder'])->name('hisorder');
    Route::get('/repass',[ProfileController::class,'showrepass'])->name('repass');
    Route::get('/detailorder/{order}',[ProfileController::class,'showOrderdetai'])->name('detailorder');

    Route::put('/up',[ProfileController::class,'handleUpdate'])->name('handleupuser');
    Route::put('/repass',[ProfileController::class,'handleRepass'])->name('handlerepass');
});
