<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use Exception;

class CartController extends Controller
{
    //
    public function index(){
        return view('cart');
    }
    
    public function addCart(Product $product, Request $request){
       
        $request->validate([
            'num'=> 'required|numeric|min:0',
        ]);

        $item = [
            'product' => $product,
            'num' => $request->num,
        ];

        if(session()->has('cart')){
            $cart = session()->get('cart');
            $ischeck = false;
            foreach($cart as $key=>$value){
                if($cart[$key]['product'] == $product){
                    $cart[$key]['num'] += $request->num;
                    $ischeck = true;
                    break;
                }
            }
            if(!$ischeck){
                $cart[count($cart)] = $item;
                session()->put('cart',$cart);
            } else{
                session()->put('cart',$cart);
            }
        }else{
            session()->put('cart', [0 => $item]);
        }

        $count = 0;
        foreach (session('cart') as $item) {
            $count += $item['num'];
        }
        
        return response()->json(['count'=>$count]);
        
    }

    public function updateToCart(Product $product, Request $request){

        $request->validate([
            'num'=> 'required|numeric|min:0',
        ]);

        $num = $request->num;
        if(session()->has('cart')){
            $cart = session()->get('cart');           
            foreach($cart as $key=>$value){
                if($cart[$key]['product'] == $product){
                    $cart[$key]['num'] = $num;
                    if($num <= 0){
                        unset($cart[$key]);
                        $cart = array_values($cart);
                    }
                    break;
                }
            }
            session()->put('cart', $cart);
        }
        $totalmoney = 0;
        foreach (session('cart') as $item) {
            $totalmoney += $item['product']['discount']*$item['num'];
        }

        $total = $product->discount*$num;

        return response()->json([
            'quantity' => $num,
            'total'=>$total,
            'totalmoney'=>$totalmoney,
            'cart_empty'=>count(session('cart')),
        ]);
    }

    public function showCheck(){
        return view('checkout');
    }

    public function handleCheck(Request $request){

        $request->validate([
            'name'=> 'required|string|max:255',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone_number' => ['required', 'string', 'regex:/^[0-9]{10,}$/'],
            'description' => 'nullable|string'
        ]);

        $orderDate = date('Y-m-d H:i:s');
        $cart = session()->get('cart');
        $orderDetails = [];
        $totalMoney = 0;
        for($i=0; $i<count($cart); $i++){
            $orderDetails[$i] = ['product_id'=> $cart[$i]['product']['id'],
                                 'price' => $cart[$i]['product']['discount'],
                                 'num' => $cart[$i]['num'],
                                 'total_money' => ($cart[$i]['num']*$cart[$i]['product']['discount']) 
                                ];
            $totalMoney += $cart[$i]['num']*$cart[$i]['product']['discount'];
        }

        DB::beginTransaction();
        try{
            $order = Order::create([
                'user_id' => Auth::user()->id,
                'name' => $request->name,
                'address' => $request->address,
                'phone_number' => $request->phone_number,
                'description' => $request->description,
                'total_money' => $totalMoney,
                'order_date' => $orderDate,
                'status' => 0,
            ]);
            $order->orderDetail()->createMany($orderDetails);
            DB::commit();
        }catch (\Exception $e) {           
            DB::rollBack();
            return redirect()->back()->withErrors('Đặt hàng không thành công');
        }

        session()->forget('cart');

        return redirect( route('cart') );
    }
}
