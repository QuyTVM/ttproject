<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    
    public function deleteImageSaved(Category $category){
        $image = $category->image;
        $newImage = str_replace('/storage/', 'public/', $image); 
        if (Storage::exists($newImage)) {
            Storage::delete($newImage);
        }
    }

    public function show(){
        $category = Category::paginate(12);
        $categoryResource = CategoryResource::collection($category);
        return view('admin.category.show',[
            'categories'=>$categoryResource
        ]);
    }

    public function edit(Category $category){
        $categoryResource = new CategoryResource($category);
        return view('admin.category.edit',[
            'categories'=>$categoryResource
        ]);
    }

    public function create(){
        return view('admin.category.create');
    }

    public function insert(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'image'=> 'required|image|mimes:jpeg,png,jpg,gif,webp'
        ]);
        
        $imageURL = '';
        $file = $request->file('image');
        $fileName = 'cates/' .time() . '_' . $file->getClientOriginalName();
        $file->storeAs('public', $fileName);

        $imageURL = Storage::url($fileName);
        

        Category::create([
            'name' => $request->name,
            'image' => $imageURL,
        ]);

        return redirect()->route('admin.category.home');
    }

    public function update(Category $category,Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'image'=> 'nullable|image|mimes:jpeg,png,jpg,gif,webp'
        ]);

        $updateData = ['name' => $request->name];

        if ($request->hasFile('image')) {
            $imageURL = '';
            $this->deleteImageSaved($category);

            $file = $request->file('image');
            $fileName = 'cates/' .time() . '_' . $file->getClientOriginalName();
            $file->storeAs('public', $fileName);

            $imageURL = Storage::url($fileName);
            $updateData['image'] = $imageURL; 
        }

        $category->update($updateData);

        return redirect()->route('admin.category.home');
    }

    public function destroy(Category $category){
        $category->delete();
        return response()->json(['message' => 'Product deleted successfully.']);
    }
}
