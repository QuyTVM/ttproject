<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    const CATEGORIES_LIMIT = ['Smartphone','Laptop'];
    const NUMBER_SHOW_PRODUCTS = 5;
    
    public function home(){

        $productDt = Category::whereIn('name', self::CATEGORIES_LIMIT)
        ->with(['products' => function ($query) {
            $query->latest();
        }])->get();

        $productDt->transform(function (Category $category) {
            $category->setRelation('products', $category->products->take(self::NUMBER_SHOW_PRODUCTS));
            return $category;
        });

        //dd($productDt);
        
        $category = Category::all();

        return view('home',[
            'productDt' => $productDt,
            'categories'=> $category
        ]);
        
    }

    public function product(Product $product){

        $categoryId = $product->category_id;

        $comment = $product->comments()->whereHas('user', function ($query) {
                        $query->whereNull('deleted_at');
                    })->where('status',0)->paginate(4);

        $productList =Category::where('id', $categoryId)
        ->with(['products' => function ($query) {
            $query->latest()->limit(self::NUMBER_SHOW_PRODUCTS);
        }])->get();

        return view('detail',[
            'listpro' => $productList,
            'product' => $product,
            'comment' => $comment
        ]);
    }
 
}

