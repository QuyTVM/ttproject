<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    //show form login
    public function showLogin(){
        return view('auth.login');
    }

    //show form register
    public function showRegister(){
        return view('auth.register');
    }


    //xử lý đăng nhập 
    public function handleLogin(Request $request){

        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        //login
        if (Auth::attempt(['email'=> $request->email,'password' => $request->password],$request->remember)){
            
            if (Auth::user()->deleted_at != NULL){
                return redirect()->route('login');
            }
            
            $request->session()->regenerate();

            if (Auth::user()->is_admin == true){      //is_admin = true úng với admin
                return redirect()->route('admin.home');
            }
            
            if (Auth::user()->is_admin == false){      //is_admin = false úng với user
                return redirect()->route('home');
            }
        }

        return back()->withErrors([
            'email' => 'Email hoặc Password không chính xác',
        ]);
    }


    // xử lý đăng ký
    public function register(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
        ]); 


        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->sdt,
            'is_admin' => false,
            'password' => Hash::make($request->password),
        ]);

        //login
        Auth::login($user);

        return redirect()->route('home');
    }

    //logout
    public function logout(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        // $request->session()->regenerateToken();

        return redirect('/home');
    }
}
