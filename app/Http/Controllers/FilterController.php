<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    //
    const MAX_PRICE = 999999999;

    public function index(Category $cate,Request $request){

        $request->validate([
            'brand'=> 'nullable|string|min:0|max:255',
            'maxprice'=> 'nullable|string|min:0|max:255',
            'minprice'=> 'nullable|string|min:0|max:255',
            'sort'=> 'nullable|string|min:0|max:255'
        ]);

        $categoryId = $cate->id;

        $queryFilter = Product::where('category_id',$categoryId);

        $brand = $queryFilter->distinct()->pluck('brand')->toArray();

        if(!empty($request->brand)){
            $queryFilter = $queryFilter->where('brand',$request->brand);
        }

        if(!empty($request->maxprice) && !empty($request->minprice)){
            $queryFilter = $queryFilter->whereBetween('discount', [$request->minprice, $request->maxprice]);
        }

        if(!empty($request->sort)){         
            $queryFilter = $queryFilter->orderBy('discount', $request->sort);
        }

        $queryFilter = $queryFilter->get();
         
        return view('filter',[
            'listpro' => $queryFilter,
            'category' => $cate,
            'brand' => $brand
        ]);
    }

}
