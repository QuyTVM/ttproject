<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;

class ProfileController extends Controller
{
    //show ra thông tin cá nhân
    public function index(){
        return view('profile.account');
    }

    //show form update
    public function showformupdate(){
        return view('profile.profile');
    }

    //show ra danh sách đơn hàng
    public function showorder(){
        $id = Auth::user()->id;
        $orderList = Order::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(4);
        return view('profile.hisorder',[
            'orderList'=>$orderList
        ]);
    }

    //show ra orderdetail
    public function showOrderdetai(Order $order){
        $listOrdetail = $order->orderDetail;
        return view('profile.detail',[
            'order' => $order,
            'listDetail'=>$listOrdetail
        ]);
    }

    //show ra form đặt lại pass
    public function showrepass(){
        return view('profile.repass');
    }



    //cập nhật thông tin cá nhân
    public function handleUpdate(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => ['required','email','max:255',Rule::unique('users')->ignore(Auth::user()->id)],
            'address' => 'nullable|string|max:255',
            'phone_number' => ['required', 'string', 'regex:/^[0-9]{10,}$/',
                                Rule::unique('users')->ignore(Auth::user()->id)],
        ],[
            'email.unique'=>'Email đã tồn tại trong hệ thống',
            'phone_number.unique'=>'Số điện thoại đã tồn tại trong hệ thống',
        ]);
     
        $user_id = Auth::user()->id;
        User::where('id',$user_id)->update([
            'name'=> $request->name,
            'email'=> $request->email,
            'address'=> $request->address,
            'phone_number'=> $request->phone_number
        ]); 
        return redirect(route('profile'));     
    } 

    //đặt lại mật khẩu
    public function handleRepass(Request $request){
        $request->validate([
            'pwd_old'=>'required|min:8',
            'pwd_new'=>'required|min:8',
        ]);

        $user = Auth::user();

        if (Auth::attempt(['id'=> $user->id,'password' => $request->pwd_old])){
            if($request->pwd_old == $request->pwd_new){
                return back()->with('pass', 'Mật khẩu mới phải khác mật khẩu cũ!');
            }else{
                $user->update([
                    'password'=> Hash::make($request->pwd_new),
                ]);
                return back()->with('success', 'Đặt lại mật khẩu thành công!');
            }
        } else{
            return back()->with('password', 'Password không chính xác!');
        }
    }
}
