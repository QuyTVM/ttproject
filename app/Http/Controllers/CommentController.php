<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CommentController extends Controller
{
    //
    public function store(Product $product,Request $request){
        $request->validate([
            'content' => 'required|string|max:255',
            // 'product_id' => 'required|numeric|min:1',
        ]);

        $user = Auth::user();

        $comment = Comment::create([
            'content' => $request->content,
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);

        return response()->json(['content' => $comment->content,
                            'name'=>$comment->user->name,
                            'created_at'=>$comment->created_at]);
    }
}
