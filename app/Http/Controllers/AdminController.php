<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function showAdmin(){
        $totalPro = Product::count();
        $totalUser = User::count();
        $totalOrder = Order::where('status', 0)->count();
        $totalCate = Category::count();
        return view('admin.home',[
            'totalPro'=>$totalPro,
            'totalUser'=>$totalUser,
            'totalOrder'=>$totalOrder,
            'totalCate'=>$totalCate
        ]);
    }

    public function showUser(){
        $users = User::paginate(15);
        return view('admin.user.showUser',[
            'users'=>$users
        ]);
    }

    public function showCreateUser(){
        return view('admin.user.createUser');
    }

    public function showEditUser(User $user){
        return view('admin.user.editUser',[
            'user'=>$user
        ]);
    }

    
    public function handleAddUser(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|max:255|unique:users',
            'is_admin' => 'required',
            'password' => 'required|min:8|confirmed',
            'phone_number' => 'nullable|string|regex:/^[0-9]{10,}$/|unique:users',
            'address' => 'nullable|string',
        ]); 

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'is_admin' => $request->is_admin,
            'address' => $request->address,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('admin.user.home');
    }


    public function handleEditUser(User $user,Request $request){
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => ['required','email','max:255',Rule::unique('users')->ignore($user)],
            'is_admin' => 'required',
            'phone_number' => ['required', 'string', 'regex:/^[0-9]{10,}$/',
                                Rule::unique('users')->ignore($user)],
            'address' => 'nullable|string',
        ],[
            'email.unique'=>'Email đã tồn tại trong hệ thống',
            'phone_number.unique'=>'Số điện thoại đã tồn tại trong hệ thống',
        ]); 
      
        User::where('id',$user->id)->update([
            'name'=> $request->name,
            'email'=> $request->email,
            'is_admin' => $request->is_admin,
            'address'=> $request->address,
            'phone_number'=> $request->phone_number
        ]); 

        return redirect()->route('admin.user.home');
    }

    public function destroy(User $user){
        //không cho người dùng xóa tài khoản đang dùng trong phiên đăng nhập
        
        $this->authorize('delete', $user);
        $user->delete();
        return response()->json(['message' => 'Tài khoản đã được xóa thành công.']);
        
    }
}
