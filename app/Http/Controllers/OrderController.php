<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderDetailResource;
use App\Http\Resources\CommentResource;

class OrderController extends Controller
{
    //
    public function show(){
        $order = Order::whereHas('user', function ($query) {
            $query->whereNull('deleted_at');
        })->orderBy('status','asc')->orderBy('order_date','desc')->paginate(12);
        $orderResource = OrderResource::collection($order);
        return view('admin.order.show',[
            'orders' => $orderResource
        ]);
    }

    public function updateStatus(Order $order,Request $request){
        $request->validate([
            'status' => 'required|numeric',
        ]);

        $order->update(['status'=>$request->status]);
        return response()->json(['message'=>'Cập nhật trạng thái thành công']);
    }

    public function showOrderDetail(Order $order){
        $orderDetail = $order->orderDetail;
        $orderDetailResource = OrderDetailResource::collection($orderDetail);
        $orderResource = new OrderResource($order);
        return view('admin.order.showDetail',[
            'order' => $orderResource,
            'OrderDetail' => $orderDetailResource
        ]);
    }

    public function showComment(){
        $comments = Comment::whereHas('user', function ($query) {
            $query->withoutTrashed(); 
        })->whereHas('product', function ($query) {
            $query->withoutTrashed(); 
        })->latest()->paginate(20);
        
        $commentResource = CommentResource::collection($comments);
        return view('admin.comment.show',[
            'comments'=>$commentResource
        ]);
    }
    
    public function changeStatusComment(Comment $comment,Request $request){
        $request->validate([
            'status'=>'required|numeric|min:0'
        ]);
        $comment->update(['status'=>$request->status]);
        return response()->json(['message'=>'cập nhật trạng thái thành công!']);
    }

    public function deleteComment(Comment $comment){
        $comment->delete();
        return response()->json(['message' => 'Comment deleted successfully.']);
    }
}
