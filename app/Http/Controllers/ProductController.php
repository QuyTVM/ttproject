<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Category;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    
    public function deleteImageSaved(Product $product){
        $image = $product->image;
        $newImage = str_replace('/storage/', 'public/', $image); 
        if (Storage::exists($newImage)) {
            Storage::delete($newImage);
        }
    }


    public function showProduct(){
        $products = Product::whereHas('category', function ($query) {
                        $query->whereNull('deleted_at');
                    })->latest()->paginate(12);
        $productResource = ProductResource::collection($products);
        return view('admin.product.showProduct',[
            'products'=>$productResource
        ]);
    }

   
    public function showCreateProduct(){
        $category = Category::all();
        $categoryResource = CategoryResource::collection($category);
        return view('admin.product.createProduct',[
            'categories'=>$categoryResource
        ]);
    }

    
    public function showEditProduct(Product $product){
        $category = Category::all();
        $categoryResource = CategoryResource::collection($category);
        $productResource = new ProductResource($product);
        return view('admin.product.editProduct',[
            'product'=>$productResource,
            'categories'=>$categoryResource
        ]);
    }

    
    public function handleAddProduct(Request $request){
        $request->validate([
            'title' => 'required|string|max:255',
            'category_id' => 'required|numeric|exists:categories,id',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,webp',
            'brand' => 'required|string|max:255',
            'description' => 'required|string'
        ]);

        $imageURL = '';
    
        $file = $request->file('image');
        $fileName = 'images/' .time() . '_' . $file->getClientOriginalName();
        $file->storeAs('public', $fileName);
        $imageURL = Storage::url($fileName);
    
        Product::create([
            'title' => $request->title,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'discount' => $request->discount,
            'image' => $imageURL,
            'brand' => $request->brand,
            'description' => $request->description
        ]);

        return redirect()->route('admin.product.home');
    }

    
    public function handleEditProduct(Product $product,Request $request){
        $request->validate([
            'title' => 'required|string|max:255',
            'category_id' => 'required|numeric|exists:categories,id',
            'price' => 'required|numeric',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp',
            'discount' => 'required|numeric',
            'brand' => 'required|string|max:255',
            'description' => 'required|string'
        ]);

        $updateData = ['title' => $request->title,
                    'category_id' => $request->category_id,
                    'price' => $request->price,
                    'discount' => $request->discount,
                    'brand' => $request->brand,
                    'description' => $request->description];

        if ($request->hasFile('image')) {
            $imageURL = '';
            $this->deleteImageSaved($product);

            $file = $request->file('image');
            $fileName = 'images/' .time() . '_' . $file->getClientOriginalName();
            $file->storeAs('public', $fileName);

            $imageURL = Storage::url($fileName);
            $updateData['image'] = $imageURL;          
        } 
        
        $product->update($updateData);

        return redirect()->route('admin.product.home');
    }

    
    public function destroy(Product $product){
        $product->delete();
        return response()->json(['message' => 'Product deleted successfully.']);
    }
}
