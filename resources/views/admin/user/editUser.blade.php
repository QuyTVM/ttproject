{{-- <h1>Trang chủ admin</h1>

<a href="{{route('logout')}}">Thoát</a> --}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

</head>
<body>

    <div id="mySidenav" class="sidenav">
        <p class="logo"><span>M</span>- Pocomart</p>
        <a href="{{route('admin.home')}}" class="icon-a"><i class="fa fa-dashboard icons"></i>&nbsp;&nbsp;Dashboard</a>
        <a href="{{route('admin.user.home')}}" class="icon-a"><i class="fa fa-users icons"></i>&nbsp;&nbsp;Users</a>
        <a href="{{route('admin.product.home')}}" class="icon-a"><i class="fa fa-product-hunt icons"></i>&nbsp;&nbsp;Product</a>
        <a href="{{route('admin.order.home')}}" class="icon-a"><i class="fa fa-shopping-bag icons"></i>&nbsp;&nbsp;Orders</a>
        <a href="{{route('admin.category.home')}}" class="icon-a"><i class="fa fa-list-alt icons"></i>&nbsp;&nbsp;Categories</a>
        <a href="{{route('admin.comment.home')}}" class="icon-a"><i class="fa fa-comments icons"></i>&nbsp;&nbsp;Comments</a>
    </div>

    <div id="main">
        <div class="head">
            <div class="col-div-6">
                <span class="nav" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
                <span class="nav2" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
            </div>

            <div class="col-div-6">

                <div class="profile">
                    <img src="../../../image/img-user.png" class="pro-img">
                    <p>{{Auth::user()->name}} <span><a href="{{route('logout')}}">Đăng xuất</a></span></p>
                </div>  
            </div>

            <div class="clearfix"></div>
            <br/><br/>

            <div class="col-div-8">
                <div class="box-8">
                    <div class="content-box">
                        <div class="form">
                            <form action="{{route('admin.user.showedit',['user'=>$user->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Name</label><br/>                                    
                                        <input type="text"  name="name" value="{{$user->name}}" required>                                
                                </div> 
                                <div class="mb-3">                                   
                                        <label for="pwd" class="form-label">Email</label><br/>                                    
                                        <input type="email"  name="email" value="{{$user->email}}" required>                            
                                </div>
                                @error('email')
                                    <span style="color:red">{{$message}}</span>
                                @enderror
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Phone number</label><br/>                                    
                                        <input type="text"  name="phone_number" value="{{$user->phone_number}}" required>                            
                                </div>
                                @error('phone_number')
                                    <span style="color:red">{{$message}}</span>
                                @enderror
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Address</label><br/>                                       
                                        <input type="text"  name="address" value="{{$user->address}}">                            
                                </div>
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Role</label><br/>                                      
                                        <select type="text"  name="is_admin" required>
                                            @if($user->is_admin == 0)
                                            <option value="0" selected>User</option>
                                            <option value="1">Admin</option>
                                            @elseif($user->is_admin == 1)
                                            <option value="0">User</option>
                                            <option value="1" selected>Admin</option>
                                            @endif
                                        </select>                            
                                </div>
                                {{-- <div class="mb-3">                                  
                                        <label for="pwd" class="form-label">Password</label> <br/>                                     
                                        <input type="password" id="pwd" name="password" value="{{$user->password}}" required>                          
                                </div>
                                <div class="mb-3">                                   
                                        <label for="pwd" class="form-label">Confirm Password</label><br/>                              
                                        <input type="password" id="confirmation_pwd" name="password_confirmation" value="{{$user->password}}" required>                            
                                </div>   --}}
                                <div class="mb-3">
                                    <button type="submit">SAVE</button>
                                </div>
                           </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="col-div-4">
                <div class="box-4">
                    <div class="content-box">
                        <p>Top Selling Products <span>View All</span></p>

                        <div class="circle">
                            <div class="mask full">
                                <div class="fill"></div>
                            </div>
                            <div class="mask half">
                                <div class="fill"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  --}}

            <div class="cleearfix"></div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(".nav").click(function(){
            $("#mySidenav").css('width','70px');
            $("#main").css('margin-left','70px');
            $(".logo").css('visibility','hidden');
            $(".logo span").css('visibility','visible');
            $(".logo span").css('margin-left','-10px');
            $(".icon-a").css('visibility','hidden');
            $(".icons").css('visibility','visible');
            $(".icons").css('margin-left','-5px');
            $(".nav").css('display','none');
            $(".nav2").css('display','block');
        })

        $(".nav2").click(function(){
            $("#mySidenav").css('width','300px');
            $("#main").css('margin-left','300px');
            $(".logo").css('visibility','visible');
            $(".logo span").css('visibility','visible');
            $(".icon-a").css('visibility','visible');
            $(".icons").css('visibility','visible');
            $(".nav").css('display','block');
            $(".nav2").css('display','none');
        })

        // function validateForm() {
        //     $pwd = $('#pwd').val();
        //     $confirmPwd = $('#confirmation_pwd').val();
        //     if($pwd != $confirmPwd){
        //         alert("Mật khẩu không khớp, vui lòng kiểm tra lại")
        //         return false
        //     }
        //     return true
        // }

    </script>
</body>
</html>