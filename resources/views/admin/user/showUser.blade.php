{{-- <h1>Trang chủ admin</h1>

<a href="{{route('logout')}}">Thoát</a> --}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">
    <script src="{{url('https://code.jquery.com/jquery-3.6.0.min.js')}}"></script>
</head>
<body>

    <div id="mySidenav" class="sidenav">
        <p class="logo"><span>M</span>- Pocomart</p>
        <a href="{{route('admin.home')}}" class="icon-a"><i class="fa fa-dashboard icons"></i>&nbsp;&nbsp;Dashboard</a>
        <a href="{{route('admin.user.home')}}" class="icon-a"><i class="fa fa-users icons"></i>&nbsp;&nbsp;Users</a>
        <a href="{{route('admin.product.home')}}" class="icon-a"><i class="fa fa-product-hunt icons"></i>&nbsp;&nbsp;Product</a>
        <a href="{{route('admin.order.home')}}" class="icon-a"><i class="fa fa-shopping-bag icons"></i>&nbsp;&nbsp;Orders</a>
        <a href="{{route('admin.category.home')}}" class="icon-a"><i class="fa fa-list-alt icons"></i>&nbsp;&nbsp;Categories</a>
        <a href="{{route('admin.comment.home')}}" class="icon-a"><i class="fa fa-comments icons"></i>&nbsp;&nbsp;Comments</a>
    </div>

    <div id="main">
        <div class="head">
            <div class="col-div-6">
                <span class="nav" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
                <span class="nav2" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
            </div>

            <div class="col-div-6">

                <div class="profile">
                    <img src="../../image/img-user.png" class="pro-img">
                    <p>{{Auth::user()->name}} <span><a href="{{route('logout')}}">Đăng xuất</a></span></p>
                </div>  
            </div>

            <div class="clearfix"></div>
            <br/><br/>

            <div class="col-div-8">
                <div class="box-8">
                    <div class="content-box">
                        <a href="{{route('admin.user.showcreate')}}" style="text-decoration: none;color:black">
                            <i class="fa-solid fa-user-plus"></i>&nbsp;&nbsp; <span>Add user</span>
                        </a>
                        <br/><br/>
                        <p>User List</p>
                        <table>
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>Email</th>
                                    <th>Quyền</th>
                                    <th>Ngày tạo</th>
                                    <th>Ngày cập nhật</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            @foreach ($users as $key=>$item)
                            <tbody>
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>@if ($item->is_admin == 1) Quản trị viên @else Người dùng @endif</td>
                                    <td>{{ \Carbon\Carbon::parse($item->created_at)->format('H:i d-m-Y') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->updated_at)->format('H:i d-m-Y') }}</td>
                                    <td>
                                        <a href="{{route('admin.user.showedit',['user'=>$item->id])}}" 
                                            style="text-decoration: none;color:black"><i class="fa-solid fa-user-pen"></i>&nbsp;&nbsp;</a>
                                        <a href="#" onclick="deleteUser({{ $item->id }})" style="text-decoration: none; color: black">
                                            <i class="fa-solid fa-eraser"></i>&nbsp;&nbsp;
                                        </a>
                                    </td>
                                </tr>
                            </tbody>   
                            @endforeach
                        </table>
                    </div>
                    <div class="pagination">
                        @if ($users->currentPage() > 1)
                            <a href="{{ $users->url(1) }}" class="page-link">First</a>
                            <a href="{{ $users->previousPageUrl() }}" class="page-link">Previous</a>    
                        @endif
                    
                        @for ($i = max(1, $users->currentPage() - 3); $i <= min($users->lastPage(), $users->currentPage() + 3); $i++)
                            @if ($i == $users->currentPage())
                                <span class="page-link current">{{ $i }}</span>
                            @else
                                <a href="{{ $users->url($i) }}" class="page-link">{{ $i }}</a>
                            @endif
                        @endfor
                    
                        @if ($users->currentPage() < $users->lastPage())               
                            <a href="{{ $users->nextPageUrl() }}" class="page-link">Next</a>                            
                            <a href="{{ $users->url($users->lastPage()) }}" class="page-link">Last</a>
                        @endif
                    </div>
                </div>
            </div>

            
            <div class="cleearfix"></div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(".nav").click(function(){
            $("#mySidenav").css('width','70px');
            $("#main").css('margin-left','70px');
            $(".logo").css('visibility','hidden');
            $(".logo span").css('visibility','visible');
            $(".logo span").css('margin-left','-10px');
            $(".icon-a").css('visibility','hidden');
            $(".icons").css('visibility','visible');
            $(".icons").css('margin-left','-5px');
            $(".nav").css('display','none');
            $(".nav2").css('display','block');
        })

        $(".nav2").click(function(){
            $("#mySidenav").css('width','300px');
            $("#main").css('margin-left','300px');
            $(".logo").css('visibility','visible');
            $(".logo span").css('visibility','visible');
            $(".icon-a").css('visibility','visible');
            $(".icons").css('visibility','visible');
            $(".nav").css('display','block');
            $(".nav2").css('display','none');
        })

        function deleteUser(userId){
            if (confirm('Bạn có muốn xóa tài khoản này không?')) {
                $.ajax({
                    type: 'DELETE',
                    url: '/admin/user/delete/' + userId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        alert(response.message);
                        location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                        alert('Đã xảy ra lỗi khi xóa tài khoản.');
                    }
                });
            }
        }
    </script>
</body>
</html>