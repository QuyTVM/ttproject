{{-- <h1>Trang chủ admin</h1>

<a href="{{route('logout')}}">Thoát</a> --}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">
    <script src="{{url('https://code.jquery.com/jquery-3.6.0.min.js')}}"></script>
</head>
<body>

    <div id="mySidenav" class="sidenav">
        <p class="logo"><span>M</span>- Pocomart</p>
        <a href="{{route('admin.home')}}" class="icon-a"><i class="fa fa-dashboard icons"></i>&nbsp;&nbsp;Dashboard</a>
        <a href="{{route('admin.user.home')}}" class="icon-a"><i class="fa fa-users icons"></i>&nbsp;&nbsp;Users</a>
        <a href="{{route('admin.product.home')}}" class="icon-a"><i class="fa fa-product-hunt icons"></i>&nbsp;&nbsp;Product</a>
        <a href="{{route('admin.order.home')}}" class="icon-a"><i class="fa fa-shopping-bag icons"></i>&nbsp;&nbsp;Orders</a>
        <a href="{{route('admin.category.home')}}" class="icon-a"><i class="fa fa-list-alt icons"></i>&nbsp;&nbsp;Categories</a>
        <a href="{{route('admin.comment.home')}}" class="icon-a"><i class="fa fa-comments icons"></i>&nbsp;&nbsp;Comments</a>
    </div>

    <div id="main">
        <div class="head">
            <div class="col-div-6">
                <span class="nav" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
                <span class="nav2" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
            </div>

            <div class="col-div-6">

                <div class="profile">
                    <img src="../../image/img-user.png" class="pro-img">
                    <p>{{Auth::user()->name}} <span><a href="{{route('logout')}}">Đăng xuất</a></span></p>
                </div>  
            </div>

            <div class="clearfix"></div>
            <br/><br/>

            <div class="col-div-8">
                <div class="box-8">
                    <div class="content-box">
                        <a href="{{route('admin.category.create')}}" style="text-decoration: none;color:black">
                            <i class="fa-solid fa-list-check"></i>&nbsp;&nbsp; <span>Add Category</span>
                        </a>
                        <br/><br/>
                        <p>Category List</p>
                        <table>
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Ảnh</th>
                                    <th>Tên</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $key=>$item)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td><img src="{{$item->image}}"></td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        <a href="{{route('admin.category.edit',['category'=>$item->id])}}"
                                            style="text-decoration: none;color:black"><i class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;
                                        </a>
                                        <a href="#" onclick="deleteCategory({{ $item->id }})" style="text-decoration: none; color: black">
                                            <i class="fa-solid fa-trash-can"></i>&nbsp;&nbsp;
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination">
                        @if ($categories->currentPage() > 1)
                            <a href="{{ $categories->url(1) }}" class="page-link">First</a>
                            <a href="{{ $categories->previousPageUrl() }}" class="page-link">Previous</a>    
                        @endif
                    
                        @for ($i = max(1, $categories->currentPage() - 3); $i <= min($categories->lastPage(), $categories->currentPage() + 3); $i++)
                            @if ($i == $categories->currentPage())
                                <span class="page-link current">{{ $i }}</span>
                            @else
                                <a href="{{ $categories->url($i) }}" class="page-link">{{ $i }}</a>
                            @endif
                        @endfor
                    
                        @if ($categories->currentPage() < $categories->lastPage())               
                            <a href="{{ $categories->nextPageUrl() }}" class="page-link">Next</a>                            
                            <a href="{{ $categories->url($categories->lastPage()) }}" class="page-link">Last</a>
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="cleearfix"></div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(".nav").click(function(){
            $("#mySidenav").css('width','70px');
            $("#main").css('margin-left','70px');
            $(".logo").css('visibility','hidden');
            $(".logo span").css('visibility','visible');
            $(".logo span").css('margin-left','-10px');
            $(".icon-a").css('visibility','hidden');
            $(".icons").css('visibility','visible');
            $(".icons").css('margin-left','-5px');
            $(".nav").css('display','none');
            $(".nav2").css('display','block');
        })

        $(".nav2").click(function(){
            $("#mySidenav").css('width','300px');
            $("#main").css('margin-left','300px');
            $(".logo").css('visibility','visible');
            $(".logo span").css('visibility','visible');
            $(".icon-a").css('visibility','visible');
            $(".icons").css('visibility','visible');
            $(".nav").css('display','block');
            $(".nav2").css('display','none');
        })

        function deleteCategory(categoryId){
            if (confirm('Are you sure you want to delete this product?')) {
                $.ajax({
                    type: 'DELETE',
                    url: '/admin/category/delete/' + categoryId,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        alert(response.message);
                        location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                        alert('An error occurred while deleting the user.');
                    }
                });
            }
        }
    </script>
</body>
</html>