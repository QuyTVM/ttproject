<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

</head>
<body>

    <div id="mySidenav" class="sidenav">
        <p class="logo"><span>M</span>- Pocomart</p>
        <a href="{{route('admin.home')}}" class="icon-a"><i class="fa fa-dashboard icons"></i>&nbsp;&nbsp;Dashboard</a>
        <a href="{{route('admin.user.home')}}" class="icon-a"><i class="fa fa-users icons"></i>&nbsp;&nbsp;Users</a>
        <a href="{{route('admin.product.home')}}" class="icon-a"><i class="fa fa-product-hunt icons"></i>&nbsp;&nbsp;Product</a>
        <a href="{{route('admin.order.home')}}" class="icon-a"><i class="fa fa-shopping-bag icons"></i>&nbsp;&nbsp;Orders</a>
        <a href="{{route('admin.category.home')}}" class="icon-a"><i class="fa fa-list-alt icons"></i>&nbsp;&nbsp;Categories</a>
        <a href="{{route('admin.comment.home')}}" class="icon-a"><i class="fa fa-comments icons"></i>&nbsp;&nbsp;Comments</a>
    </div>

    <div id="main">
        <div class="head">
            <div class="col-div-6">
                <span class="nav" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
                <span class="nav2" style="font-size: 30px;cursor: pointer;color:white">
                    &#9776; Admin</span>
            </div>

            <div class="col-div-6">

                <div class="profile">
                    <img src="../../image/img-user.png" class="pro-img">
                    <p>{{Auth::user()->name}} <span><a href="{{route('logout')}}">Đăng xuất</a></span></p>
                </div>  
            </div>

            <div class="clearfix"></div>
            <br/><br/>

            <div class="col-div-8">
                <div class="box-8">
                    <div class="content-box">
                        <div class="form">
                            <form action="{{route('admin.product.add')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Title</label><br/>                                    
                                        <input type="text"  name="title" value="" required>                                
                                </div> 
                                <div class="mb-3">                                   
                                        <label for="pwd" class="form-label">Category</label><br/>                                    
                                        <select type="text"  name="category_id" required>
                                            {{-- <option value="{{$item->id}}">{{$item->name}}</option> --}}
                                            @foreach ($categories as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>                             
                                </div>
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Price</label><br/>                                    
                                        <input type="text"  name="price" value="" required>                            
                                </div>
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Discount</label><br/>                                       
                                        <input type="text"  name="discount" value="">                            
                                </div>
                                <div class="mb-3">                                    
                                        <label for="pwd" class="form-label">Image</label><br/>                                      
                                        <input type="file"  name="image" value="">                            
                                </div>
                                <div class="mb-3">                                   
                                    <label for="pwd" class="form-label">Brand</label><br/>                              
                                    <input type="text" name="brand" value="" required>                            
                                </div> 
                                <div class="mb-3">                                  
                                        <label for="pwd" class="form-label">Description</label> <br/>                                     
                                        <textarea name="description" placeholder="ghi chú" 
                                        style="max-width: 60%;min-width:60%; height: 150px;margin-top:10px"></textarea>                       
                                </div>
                                <div class="mb-3">
                                    <button type="submit">ADD</button>
                                </div>
                           </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="col-div-4">
                <div class="box-4">
                    <div class="content-box">
                        <p>Top Selling Products <span>View All</span></p>

                        <div class="circle">
                            <div class="mask full">
                                <div class="fill"></div>
                            </div>
                            <div class="mask half">
                                <div class="fill"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  --}}

            <div class="cleearfix"></div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(".nav").click(function(){
            $("#mySidenav").css('width','70px');
            $("#main").css('margin-left','70px');
            $(".logo").css('visibility','hidden');
            $(".logo span").css('visibility','visible');
            $(".logo span").css('margin-left','-10px');
            $(".icon-a").css('visibility','hidden');
            $(".icons").css('visibility','visible');
            $(".icons").css('margin-left','-5px');
            $(".nav").css('display','none');
            $(".nav2").css('display','block');
        })

        $(".nav2").click(function(){
            $("#mySidenav").css('width','300px');
            $("#main").css('margin-left','300px');
            $(".logo").css('visibility','visible');
            $(".logo span").css('visibility','visible');
            $(".icon-a").css('visibility','visible');
            $(".icons").css('visibility','visible');
            $(".nav").css('display','block');
            $(".nav2").css('display','none');
        })

        // function validateForm() {
        //     $pwd = $('#pwd').val();
        //     $confirmPwd = $('#confirmation_pwd').val();
        //     if($pwd != $confirmPwd){
        //         alert("Mật khẩu không khớp, vui lòng kiểm tra lại")
        //         return false
        //     }
        //     return true
        // }

    </script>
</body>
</html>