<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.theme.default.min.css')}}">
    <script src="{{asset('assets/vendors/jquery.min.js')}}"></script>
    <script src="{{asset('assets/owlcarousel/owl.carousel.js')}}"></script>
    <script src="{{url('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js')}}"></script>
    <script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js')}}"></script>
    <script src="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js')}}"></script>

</head>
<body>

    <section class="myheader">
        <div class="container py-3">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('image/logo.webp')}}" class="img-fluid" alt="logo">
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Từ khóa tìm kiếm" aria-label="Từ khóa tìm kiếm" aria-describedby="basic-addon2">
                        <span class="input-group-text" id="basic-addon2"><i class="fa-solid fa-magnifying-glass"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-solid fa-phone"></i>
                                    </div>
                                </div>
                                <div class="col-9">
                                    Tư vấn hỗ trợ<br>
                                    <strong class="text-danger">0987654321</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-regular fa-circle-user"></i>
                                    </div>
                                </div>
                                <?php
                                   $user = Auth::user();
                                   if(Auth::check()){ 
                                 ?>
                                <div class="col-8">
                                    <a href="{{route('profile')}}" style="text-decoration: none;color:black">
                                        <span>{{Auth::user()->name}}</span></a><br>
                                    <strong class="text-danger"><a href="{{route('logout')}}" style="text-decoration:none; color:red">Đăng xuất</a></strong>
                                </div>
                                <?php
                                }else{
                                 ?>
                                <div class="col-9">
                                    <span>Xin chào!</span><br>
                                    <strong class="text-danger"><a href="{{route('login')}}" style="text-decoration:none; color:red">Đăng nhập</a></strong>
                                </div>
                                <?php 
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        <div class="col">
                            <a href="#" class="position-relative">
                                <span class="fs-4"><i class="fa-regular fa-heart"></i></span>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                  0
                                  <span class="visually-hidden">unread messages</span>
                                </span>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--My header-->

    <section class="mymainmenu bg-danger">
        <div class="container">
            <div class="row">
                <!-- <div class="col-md-3 text-white py-3">Danh mục sản phẩm</div> -->
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg bg-danger">
                        <div class="container-fluid">
                          <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                              <li class="nav-item">
                                <a class="nav-link text-white active" aria-current="page" href="{{route('home')}}" style="margin-right:100px">Trang chủ</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="#" style="margin-right:100px">Giới thiệu</a>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" href="#" role="button"
                                 data-bs-toggle="dropdown" aria-expanded="false" style="margin-right:100px">
                                  Sản phẩm
                                </a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="#">Action</a></li>
                                  <li><a class="dropdown-item" href="#">Another action</a></li>
                                  <li><hr class="dropdown-divider"></li>
                                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Tin tức</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="{{route('cart')}}" style="margin-right:100px">Giỏ hàng</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Liên hệ</a>
                              </li>
                            </ul>
                        
                          </div>
                        </div>
                      </nav>
                </div>
            </div>
        </div>
    </section>

    <!--Ny content-->
    <section class="mymaincontent my-3">
        <div class="container" style="margin-top: 20px; margin-bottom: 20px;">
            <div class="row">
                <ul class="breadcrumb">
                @foreach ($listpro as $cate)
                    <li><a href="{{route('home')}}">Trang chủ</a></li>
                    <li><a href="detail.html"> / {{$cate->name}}</a></li>
                    <li> / {{$product->title}}</li>
                </ul>
                <hr>
                <div class="col-md-5">
                    <img src="{{$product->image}}" style="width:100%;">
                </div>
                <div class="col-md-6">
                    <h2 style="margin-left: 15px">{{$product->title}}</h2>
                    <div class="row">
                        <div class="col">
                            <h2 style="font-size: 25px; padding: 5px 0px; color: red;
                            margin-top: 5px;margin-left: 20px;">{{number_format($product->discount)}} VNĐ 
                            </h2>
                        </div>
                        <div class="col">
                            <h3 style="font-size: 15px; color: grey;margin-top: 17px;margin-right:70px">
                            <del>{{number_format($product->price)}} VNĐ</del></h3>
                        </div>
                    </div>
                    <div class="area_promo">
                        <strong>Khuyến mãi</strong>
                        <div class="promo">
                            <img src="{{asset('image/icon-tick.png')}}" >
                            <div id="detailPromo"><p style="margin: 0px;">Khách hàng có thể mua trả góp sản phẩm với lãi suất 0% với thời hạn 6 tháng kể từ khi mua hàng.</p></div>
                        </div>
                        <div class="promo">
                            <img src="{{asset('image/icon-tick.png')}}">
                            <div id="detailPromo"><p style="margin: 0px;">Giảm giá 5% khi mua phụ kiện.</p></div>
                        </div>
                        <div class="promo">
                            <img src="{{asset('image/icon-tick.png')}}">
                            <div id="detailPromo"><p style="margin: 0px;">Thu cũ đổi mới: Giá thu cao - Thủ tục nhanh chóng - Trợ giá tốt nhất.</p></div>
                        </div>
                    </div>

                    <div class="policy">
                        <div>
                            <img src="{{asset('image/box.png')}}">
                            <p>Bộ sản phẩm gồm: Hộp, Sách hướng dẫn, Cây lấy sim, Củ sạc nhanh rời đầu Type A, Cáp Type C</p>
                        </div>
                        <div>
                            <img src="{{asset('image/icon-baohanh.png')}}">
                            <p>Bảo hành chính hãng 12 tháng</p>
                        </div>
                        <div class="last">
                            <img src="{{asset('image/1-1.jpg')}}">
                            <p>1 đổi 1 trong 1 tháng nếu lỗi, đổi trong vòng 1 ngày hoặc đổi tại cửa hàng</p>
                        </div>
                    </div>
                    
                    <h3 style="margin: 15px; color:black">SỐ SẢN PHẨM</h3>
                    <div style="display: flex; margin-left: 10px;">
                        <button class="btn btn-light" style="border: solid #e0dede 1px;
                        border-radius: 0px;" onclick="addMoreCart(-1)">-</button>
                        <input type="number" name="num" class="form-control" step="1" value="1" 
                            style="max-width: 90px; border: solid #e0dede 2px;
                            border-radius: 0px; text-align: center; background-color: #f2f2f2;" onchange="fixCartNum()">
                        <button class="btn btn-light" style="border: solid #e0dede 1px;
                        border-radius: 0px;" onclick="addMoreCart(1)">+</button>
                    </div>
                    <button class="btn btn-primary" onclick="addCart({{$product->id}})" 
                    style="margin: 20px 10px; width:96%; border-radius: 3px; font-size: 15px;">
                 
                    <i class="fa fa-cart-plus"></i> THÊM VÀO GIỎ HÀNG
                    </button></a>
                </div>
            </div>
            <div class="box" style="width:75%;border:1px #e0dede solid;border-radius:8px;margin-top:100px">
                <h2 style="margin-left:15px;">Đánh giá sản phẩm</h2>
                <hr>
                <div style="margin-top:30px;margin-left:15px;margin-bottom:15px">
                    {{-- <form id="commentForm">
                        @csrf --}}
                        {{-- <input type="hidden" id="productId" name="product_id" value="{{$product->id}}"> --}}
                        <textarea id="contentRate" name="content" placeholder="Hãy để lại đánh giá của bạn" 
                        style="max-width: 800px; min-width: 800px; height: 100px;" required></textarea>
                        <p style="text-align: center; font-size: 16px; font-weight: bold;">
                            Bạn nghĩ sao về sản phẩm này?</p>
                        <div style="display: flex; justify-content: center;">
                            <button id="btnSubmit" onclick="addComment({{$product->id}})">Gửi</button>
                        </div>
                    {{-- </form> --}}
                </div>
                <div class="ct-box" style="margin-top:30px;margin-left:15px">
                    <div class="box-c" style="margin-top:30px">
                        <div id="commentBox">

                        </div>
                        <div id="commentsList">
                            @foreach ($comment as $item)
                            <p style="font-weight:600;font-size:16px;font-family:'Arial', Times, serif">
                                {{$item->user->name}}</p>
                            <p style="font-weight:400;font-size:18px;font-family:'Times New Roman', Times, serif">
                                {{$item->content}}</p>
                            <p style="font-weight:unset;font-size:12px;font-family:'Times New Roman', Times, serif">
                                {{$item->created_at}}</p>
                            @endforeach
                        </div>
                        <div class="pagination">
                            @if ($comment->currentPage() > 1)
                                <a href="{{ $comment->url(1) }}" class="page-link">First</a>
                                <a href="{{ $comment->previousPageUrl() }}" class="page-link">Previous</a>
                            @endif
                        
                            {{-- @for ($i = max(1, $comment->currentPage() - 3); $i <= min($comment->lastPage(), $comment->currentPage() + 3); $i++)
                                @if ($i == $comment->currentPage())
                                    <span class="page-link current">{{ $i }}</span>
                                @else
                                    <a href="{{ $comment->url($i) }}" class="page-link">{{ $i }}</a>
                                @endif
                            @endfor --}}
                        
                            @if ($comment->currentPage() < $comment->lastPage())
                                <a href="{{ $comment->nextPageUrl() }}" class="page-link">Next</a>
                                <a href="{{ $comment->url($comment->lastPage()) }}" class="page-link">Last</a>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <hr>

        </div>

    
        <div class="container" style="margin-top: 20px; margin-bottom: 20px;">
            <h4 style="text-align: center; margin-top: 20px; margin-bottom: 20px;">
                SẢN PHẨM LIÊN QUAN
            </h4>
            <div class="product_list-s">
                <div class="row">
                @foreach ($cate->products as $item)
                    <div class="imd col-md-3 mb-3">
                        <img src="{{$item->image}}" alt="" class="img-fluid">
                        <h3><a href="{{route('product',['product'=>$item])}}" style="text-decoration: none; color:black">{{$item->title}}</a></h3>
                        <h4>{{number_format($item->discount)}} VNĐ</h4>
                    </div>
                @endforeach                
                </div>
            </div>
            @endforeach
        </div>

    </section>
    
    <style>
        .box h2{
            display: block;
            font-size: 1.5em;
            margin-block-start: 0.83em;
            margin-block-end: 0.83em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            font-weight: bold;
            font-family:'Times New Roman', Times, serif;
        }
        #btnSubmit{
            background: #151f3c;
            color: white;
            border-radius: 3px;
            width: 110px;
            height: 45px;
            font-size: 17px;
            font-weight: bold;
            cursor: pointer;
            margin-top: 20px;
        }
        #rateContainer{
            margin-top: 10px;
        }
        #contentRate{
            font-family: arial;
            padding: 10px;
            font-size: 14px;
        }
        .starContainer{
            margin-left: 2px;
        }
    </style>

    <!--footer-->
    <section class="myfooter bg-dark text-white py-4" >
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="fs-5">Văn phòng giao dịch</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Về chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Chính sách bán hàng</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Theo dõi chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <h5>THIÊN ĐƯỜNG MUA SẮM POCO MART</h5>
                    <p class="m-0">Copyright@ 2021 Công ty cổ phần thương mại Poco Mart</p>
                    <p class="m-0">Chứng nhận ĐKKD số: 0388282938 do sở KH & ĐT TP.Hà Nội cấp</p>
                    <p class="m-0">Địa chỉ: Tòa nhà Ladeco 266 Đội Cấn, Ba Đình, Hà Nội</p>
                    <p class="m-0">Điện thoại: 19006750 - Email: support@sapo.vn</p>
                </div>
                <div class="col-md-6">
                    <h5>Nhận tin khuyến mại</h5>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <span class="input-group-text text-white bg-danger" id="basic-addon2">Đăng ký</span>
                    </div>
                    <div>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-square-facebook"></i>
                        </span>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-twitter"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-google"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-youtube"></i>
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">Bản quyền thuộc về Ego Creative Cung cấp bởi Sapo</div>
                <div class="col-md-6 text-end">
                    Trang chủ
                    Giới thiệu
                    Sản phẩm
                    Tin mới nhất
                    Câu hỏi thường gặp
                </div>
            </div>
        </div>
    </section>

    @php
    if(!session()->has('cart')){
        session()->put('cart', []);
    }
    $count = 0;
    foreach (session('cart') as $item) {
        $count += $item['num'];
    }
    @endphp

    <div class="cart_icon">
        <span id="cart" class="cart_count">{{$count}}</span>
        <a href="{{route('cart')}}"><img src="{{asset('image/cart.png')}}"></a>
    </div>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="{{url('https://code.jquery.com/jquery-3.6.0.min.js')}}"></script>
    <script>
        function addMoreCart(delta) {
		num = parseInt($('[name=num]').val())
		num += delta
		if(num < 1) num = 1;
		$('[name=num]').val(num)
	}

	function fixCartNum() {
		$('[name=num]').val(Math.abs($('[name=num]').val()))
	}

    // function addCart(productId, num) {
    //     $.post('/cart/add/' + productId, {
    //         "_token": "{{ csrf_token() }}",
    //         'num': num
    //     }, function(data){
    //         location.reload()
    //     })
    // }
    function addCart(productId) {
        $.ajax({
            type:'POST',
            url : '/cart/add/' + productId,
            data:{
                "_token": "{{ csrf_token() }}",
                'num': $('[name=num]').val()
            },
            success: function (response) {
                $('#cart').html(response.count);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        })
    }

    // $(document).ready(function () {
    //     $('#commentForm').on('submit', function (e) {
    //         e.preventDefault();
            
    //         var formData = $(this).serialize();
            
    //         $.ajax({
    //             type: 'POST',
    //             url: '/add-comment',
    //             data: formData,
    //             success: function (response) {
    //                 // Xử lý kết quả thành công
    //                 // Có thể hiển thị bình luận mới trong danh sách
    //                 $('#commentBox').prepend('<div><p style="font-weight:600;font-size:16px;font-family:Arial, Times, serif">' + response.name + '<p></div>'+
    //                                         '<div><p style="font-weight:400;font-size:18px;font-family:Times New Roman, Times, serif">'+ response.content + '<p></div>');
                    
    //                 $('textarea[name="content"]').val('');
    //             },
    //             error: function (xhr, status, error) {
    //                 // Xử lý lỗi nếu có
    //                 console.log(xhr.responseText);
    //             }
    //         });
    //     });
    // });
    function addComment(productId) {
        $.ajax({
            type: 'POST',
            url: '/add-comment/' + productId,
            data: {
                "_token": "{{ csrf_token() }}",
                "content": $('#contentRate').val(),
            },
            success: function(response) {
                // Xử lý kết quả thành công
                // Có thể hiển thị bình luận mới trong danh sách
                $('#commentBox').prepend('<div><p style="font-weight:600;font-size:16px;font-family:Arial, Times, serif">' + response.name + '</p></div>' +
                                         '<div><p style="font-weight:400;font-size:18px;font-family:Times New Roman, Times, serif">' + response.content + '</p></div>'+
                                         '<div><p style="font-weight:100;font-size:12px;font-family:Times New Roman, Times, serif">' + response.created_at + '</p></div>');

                $('#contentRate').val('');
            },
            error: function(response) {
                // Xử lý lỗi
                if (response.status === 'error') {
                    // alert(response.message);
                }
            },
        });
    }
    </script>
</body>
</html>