<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.theme.default.min.css')}}">
    <script src="{{asset('assets/vendors/jquery.min.js')}}"></script>
    <script src="{{asset('assets/owlcarousel/owl.carousel.js')}}"></script>

</head>
<body>
    <section class="myheader">
        <div class="container py-3">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('image/logo.webp')}}" class="img-fluid" alt="logo">
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Từ khóa tìm kiếm" aria-label="Từ khóa tìm kiếm" aria-describedby="basic-addon2">
                        <span class="input-group-text" id="basic-addon2"><i class="fa-solid fa-magnifying-glass"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-solid fa-phone"></i>
                                    </div>
                                </div>
                                <div class="col-9">
                                    Tư vấn hỗ trợ<br>
                                    <strong class="text-danger">0987654321</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-regular fa-circle-user"></i>
                                    </div>
                                </div>
                                <?php
                                   if(Auth::check()){ 
                                 ?>
                                <div class="col-8">
                                    <a href="{{route('profile')}}" style="text-decoration: none;color:black">
                                        <span>{{Auth::user()->name}}</span></a><br>
                                    <strong class="text-danger"><a href="{{route('logout')}}" 
                                        style="text-decoration:none;color:red">Đăng xuất</a></strong>
                                </div>
                                <?php
                                }else{
                                 ?>
                                <div class="col-9">
                                    <span>Xin chào!</span><br>
                                    <strong class="text-danger"><a href="{{route('login')}}" 
                                        style="text-decoration:none; color:red">Đăng nhập</a></strong>
                                </div>
                                <?php 
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        
                        <div class="col">
                            <a href="#" class="position-relative">
                                <span class="fs-4"><i class="fa-regular fa-message"></i></span>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                  0
                                  <span class="visually-hidden">unread messages</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--My header-->

    <section class="mymainmenu bg-danger">
        <div class="container">
            <div class="row">
                <!-- <div class="col-md-3 text-white py-3">Danh mục sản phẩm</div> -->
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg bg-danger">
                        <div class="container-fluid">
                          <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                              <li class="nav-item">
                                <a class="nav-link text-white active" aria-current="page" href="{{route('home')}}" style="margin-right:100px">Trang chủ</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="#" style="margin-right:100px">Giới thiệu</a>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" href="#" role="button"
                                 data-bs-toggle="dropdown" aria-expanded="false" style="margin-right:100px">
                                  Sản phẩm
                                </a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="#">Action</a></li>
                                  <li><a class="dropdown-item" href="#">Another action</a></li>
                                  <li><hr class="dropdown-divider"></li>
                                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Tin tức</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="{{route('cart')}}" style="margin-right:100px">Giỏ hàng</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Liên hệ</a>
                              </li>
                            </ul>
                        
                          </div>
                        </div>
                      </nav>
                </div>
            </div>
        </div>
    </section>


    <style>
        .cmd a{
            text-decoration: none;
            color: black
        }

        .cm{
            margin-left: 60px;
            margin-top: 50px;
        }
        .cm input{
            border-radius:7px;
            height: 35px;
            width: 450px;
            border: 2px #EEEEEE solid;
        }
    </style>
    <section class="mymaincontent my-3">
        <div class="container">
            <div class="row">
                <ul class="breadcrumb">                    
                    <li><a href="{{route('home')}}">Trang chủ</a></li>
                    <li><a href="detail.html"> / Tài khoản của tôi</a></li>   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-3" style="height:520px; background-color:#F5F5F5;
                box-shadow: 5px 5px 10px rgba(0,0,0, 0.7);border-radius:5px;">
                    <div class="ct" style="text-align:center">
                        <img src="../image/img-user.png" style="width:30%;margin-top:10px">                  
                        <h6>{{Auth::user()->name}}</h6>
                    </div>                           
                    <div class="cmd" style="margin-top: 60px;margin-left:40px">
                        <a href="{{route('profile')}}" class="">Thông tin cá nhân</a><br><br>
                        <a href="{{route('updateuser')}}" class="">Cập nhật thông tin </a><br><br>
                        <a href="{{route('hisorder')}}">Đơn hàng của bạn</a><br><br>
                        <a href="{{route('repass')}}" class="">Đổi mật khẩu</a><br><br>
                        <a href="{{route('logout')}}" class="">Đăng suất</a><br><br>
                    </div> 
               </div>
               <div class="col-md-8" style="background-color:#F5F5F5;
               height:520px; width:776px;margin-left:40px;border-radius:5px;
               box-shadow: 5px 5px 10px rgba(0,0,0, 0.7);">
                    <div class="cmt" style="text-align:center">
                        <img src="../image/img-user.png" style="width:12%;margin-top:20px">    
                    </div>
                    <div class="cm">                
                        <div class="mb-3">
                            <label for="name" class="form-label" style="margin-right: 80px">Họ và tên</label>
                            <input type="name" name="name" value="{{Auth::user()->name}}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label" style="margin-right: 109px">Email</label>
                            <input type="email" name="email" value="{{Auth::user()->email}}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label" style="margin-right: 55px">Số điện thoại</label>
                            <input type="text"  name="phone_number" value="{{Auth::user()->phone_number}}"  disabled>
                        </div>
                        <div class="mb-3">
                            <label for="add" class="form-label" style="margin-right: 100px">Địa chỉ</label>
                            <input type="text" name="address" value="{{Auth::user()->address}}" disabled>
                        </div>
                    </div>
               </div>
            </div>                            
        </div>
    </section>


    <section class="myfooter bg-dark text-white py-4"  style="margin-top:80px">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="fs-5">Văn phòng giao dịch</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Về chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Chính sách bán hàng</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Theo dõi chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <h5>THIÊN ĐƯỜNG MUA SẮM POCO MART</h5>
                    <p class="m-0">Copyright@ 2021 Công ty cổ phần thương mại Poco Mart</p>
                    <p class="m-0">Chứng nhận ĐKKD số: 0388282938 do sở KH & ĐT TP.Hà Nội cấp</p>
                    <p class="m-0">Địa chỉ: Tòa nhà Ladeco 266 Đội Cấn, Ba Đình, Hà Nội</p>
                    <p class="m-0">Điện thoại: 19006750 - Email: support@sapo.vn</p>
                </div>
                <div class="col-md-6">
                    <h5>Nhận tin khuyến mại</h5>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <span class="input-group-text text-white bg-danger" id="basic-addon2">Đăng ký</span>
                    </div>
                    <div>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-square-facebook"></i>
                        </span>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-twitter"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-google"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-youtube"></i>
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">Bản quyền thuộc về Ego Creative Cung cấp bởi Sapo</div>
                <div class="col-md-6 text-end">
                    Trang chủ
                    Giới thiệu
                    Sản phẩm
                    Tin mới nhất
                    Câu hỏi thường gặp
                </div>
            </div>
        </div>
    </section>


    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>