<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css')}}" 
           integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" 
           crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/assets/owl.theme.default.min.css')}}">
    <script src="{{asset('assets/vendors/jquery.min.js')}}"></script>
    <script src="{{asset('assets/owlcarousel/owl.carousel.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

</head>
<body>

    <section class="myheader">
        <div class="container py-3">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('image/logo.webp')}}" class="img-fluid" alt="logo">
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Từ khóa tìm kiếm" aria-label="Từ khóa tìm kiếm" aria-describedby="basic-addon2">
                        <span class="input-group-text" id="basic-addon2"><i class="fa-solid fa-magnifying-glass"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-solid fa-phone"></i>
                                    </div>
                                </div>
                                <div class="col-9">
                                    Tư vấn hỗ trợ<br>
                                    <strong class="text-danger">0987654321</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <div class="fs-3 text-danger">
                                        <i class="fa-regular fa-circle-user"></i>
                                    </div>
                                </div>
                                <?php
                                   $user = Auth::user();
                                   if(Auth::check()){ 
                                 ?>
                                <div class="col-8">
                                    <a href="{{route('profile')}}" style="text-decoration: none;color:black">
                                        <span>{{Auth::user()->name}}</span></a><br>
                                    <strong class="text-danger"><a href="{{route('logout')}}" style="text-decoration:none; color:red">Đăng xuất</a></strong>
                                </div>
                                <?php
                                }else{
                                 ?>
                                <div class="col-9">
                                    <span>Xin chào!</span><br>
                                    <strong class="text-danger"><a href="{{route('login')}}" style="text-decoration:none; color:red">Đăng nhập</a></strong>
                                </div>
                                <?php 
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="row">
                        <div class="col">
                            <a href="#" class="position-relative">
                                <span class="fs-4"><i class="fa-regular fa-heart"></i></span>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                  0
                                  <span class="visually-hidden">unread messages</span>
                                </span>
                            </a>
                        </div>
                  
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--My header-->

    <section class="mymainmenu bg-danger">
        <div class="container">           
            <div class="row">
                <!-- <div class="col-md-3 text-white py-3">Danh mục sản phẩm</div> -->
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg bg-danger">
                        <div class="container-fluid">
                          <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                              <li class="nav-item">
                                <a class="nav-link text-white active" aria-current="page" href="{{route('home')}}" style="margin-right:100px">Trang chủ</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="#" style="margin-right:100px">Giới thiệu</a>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" href="#" role="button"
                                 data-bs-toggle="dropdown" aria-expanded="false" style="margin-right:100px">
                                  Sản phẩm
                                </a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="#">Action</a></li>
                                  <li><a class="dropdown-item" href="#">Another action</a></li>
                                  <li><hr class="dropdown-divider"></li>
                                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Tin tức</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" href="{{route('cart')}}" style="margin-right:100px">Giỏ hàng</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-white" style="margin-right:100px">Liên hệ</a>
                              </li>
                            </ul>
                        
                          </div>
                        </div>
                      </nav>
                </div>
            </div>
        </div>
    </section>

    <style>
        .tble{
            margin-top:10px; 
            border: 1px solid grey
        }
        .tble th,td{
            border: 1px solid grey; 
            padding: 8px 0px;
            text-align: center;
        }
        .tble button{
            background: #202c4f; 
            color:#fff;
            border-radius: 5px;
        }
        .tble input{
            text-align: center;
            height: 30px;
            width: 80px;
        }
        .tble img{
            width: 100px;
        }
    </style>
    <!--Ny content-->
    <section class="mymaincontent my-3">
        <div class="container">
            <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                <ul class="breadcrumb">
                    <li><a href="{{route('home')}}">Trang chủ</a></li>
                    <li><a href="{{route('cart')}}"> / Giỏ hàng</a></li>
                </ul>
                <h3 style="font-size: 20px">GIỎ HÀNG CỦA BẠN</h3>
                <hr style="border:3px orangered solid">
            </div>
        
            @if (session()->has('cart') && count(session('cart')) != 0)
            @php
            $count = 0;
            $totalmoney = 0;
            @endphp
            <div class="row">
                <table class="tble">
                    <thead>
                        <th>Ảnh</th style="width:30%">
                        <th>Tên sản phẩm</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                        <th>Xoá</th>
                    </thead>
                    <tbody>
                        @foreach (session('cart') as $item)
                        <tr  class="product-row" id="row_{{$item['product']['id']}}">
                            <td><img src="{{$item['product']['image']}}"></td>
                            <td>{{$item['product']['title']}}</td>
                            <td>{{ number_format($item['product']['discount'])}} VNĐ</td>
                            <td>
                            
                            <button onclick="addMoreCart({{$item['product']['id']}},-1)"><i class="fa fa-minus"></i></button>
                            <input type="number "size="1" id="num_{{$item['product']['id']}}" 
                            onchange="fixCartNum({{$item['product']['id']}})" value="{{$item['num']}}">
                            <button onclick="addMoreCart({{$item['product']['id']}},1)"><i class="fa fa-plus"></i></button>
                            </td>
                            <td><span id="total_{{$item['product']['id']}}">{{ number_format($item['product']['discount']*$item['num'])}}</span> VNĐ</td>
                            <td>
                                <button onclick="updateCart({{$item['product']['id']}}, 0)"><i class="fa fa-trash"></i></button>
                            </td>
                            @php 
                            $totalmoney += $item['product']['discount']*$item['num'];
                            @endphp
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="money" class="row" style="margin-top: 20px;">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <h3 id="totalmoney" style="text-align: center;font-size:17px">Tổng tiền: {{number_format($totalmoney)}} VNĐ</h3>
                </div>
            </div>
            <div class="row" id="checkout" style="margin-top: 20px;">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    @if(Auth::check())
                    <a href="{{route('check')}}"><button style="width: 100%;background-color: #eb3e32; color: #fff;
                    border:solid #eb3e32 1px; border-radius:5px;text-align: center; 
                    line-height: 50px;">Thanh toán</button><a>
                    @else
                    <button style="width: 100%;background-color: #eb3e32; color: #fff;
                    border:solid #eb3e32 1px; border-radius:5px;text-align: center; 
                    line-height: 50px;" onclick="call()">Thanh toán</button>
                    @endif
                </div>
            </div>

            @else
            <style>
                .bi {
                    font-size: 100px;
                    color: red;
                }
                .icon {
                    text-align: center;
                }

            </style>
            <div class="icon">
                <span class="icon-container"><i class="bi bi-bag-x"></i></span>
                <p>Không có sản phẩm nào trong giỏ hàng của bạn</p>
            </div>
            @endif
            <div id="display">
                <style>
                    .bi {
                        font-size: 100px;
                        color: red;
                    }
                    .icon {
                        text-align: center;
                    }
    
                </style>
            </div>
        </div>

    </section>

    <!--footer-->
    <section class="myfooter bg-dark text-white py-4" style="margin-top:80px">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="fs-5">Văn phòng giao dịch</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Về chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Chính sách bán hàng</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>

                <div class="col-md-3">
                    <h4 class="fs-5">Theo dõi chúng tôi</h4>
                    <ul class="list-menu">						
						<li class="li_menu"><a href="/" title="Trang chủ">Trang chủ</a></li>						
						<li class="li_menu"><a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a></li>						
						<li class="li_menu"><a href="/collections/all" title="Sản phẩm">Sản phẩm</a></li>						
						<li class="li_menu"><a href="/tin-moi-nhat" title="Tin mới nhất">Tin mới nhất</a></li>						
						<li class="li_menu"><a href="/cau-hoi-thuong-gap" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>				
						<li class="li_menu"><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>						
						<li class="li_menu"><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>						
					</ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <h5>THIÊN ĐƯỜNG MUA SẮM POCO MART</h5>
                    <p class="m-0">Copyright@ 2021 Công ty cổ phần thương mại Poco Mart</p>
                    <p class="m-0">Chứng nhận ĐKKD số: 0388282938 do sở KH & ĐT TP.Hà Nội cấp</p>
                    <p class="m-0">Địa chỉ: Tòa nhà Ladeco 266 Đội Cấn, Ba Đình, Hà Nội</p>
                    <p class="m-0">Điện thoại: 19006750 - Email: support@sapo.vn</p>
                </div>
                <div class="col-md-6">
                    <h5>Nhận tin khuyến mại</h5>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <span class="input-group-text text-white bg-danger" id="basic-addon2">Đăng ký</span>
                    </div>
                    <div>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-square-facebook"></i>
                        </span>
                        <span class="box50 border border-danger text-primary bg-white">
                            <i class="fa-brands fa-twitter"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-google"></i>
                        </span>
                        <span class="box50 border border-danger text-danger bg-white">
                            <i class="fa-brands fa-youtube"></i>
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">Bản quyền thuộc về Ego Creative Cung cấp bởi Sapo</div>
                <div class="col-md-6 text-end">
                    Trang chủ
                    Giới thiệu
                    Sản phẩm
                    Tin mới nhất
                    Câu hỏi thường gặp
                </div>
            </div>
        </div>
    </section>

    <style>
        .product-row {
            display: table-row;
        }
    </style>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
        function addMoreCart(id, delta) {
            num = parseInt($('#num_'+id).val())
            num += delta
            $('#num_'+id).val(num)

            updateCart(id,num)
        }

        function fixCartNum(id) {
            $('#num_'+id).val(Math.abs($('#num_'+id).val()))

            updateCart(id,$('#num_'+id).val())
        }

        // function updateCart(productId, num) {
        //     $.post("/cart/update/" + productId,{
        //         "_token": "{{ csrf_token() }}",
        //         "num": num,
        //         },function(data){
        //             location.reload();
        //         }
        //     );
        // }

        function updateCart(productId,num){
            $.ajax({
                type:"POST",
                url : "/cart/update/" + productId,
                data:{
                    "_token": "{{ csrf_token() }}",
                    "num": num,
                },
                success: function (response) {
                    if (response.cart_empty === 0) {
                        $('.tble').remove();
                        $('#money').remove();
                        $('#checkout').remove(); 
                        $('#display').append('<div class="icon">'+
                                                '<span class="icon-container"><i class="bi bi-bag-x"></i></span>'+
                                                '<p>Không có sản phẩm nào trong giỏ hàng của bạn</p>'+
                                              '</div>')
                    } else{
                        if(parseInt(response.quantity) <= 0){
                            $('#row_' + productId).remove();
                            $('#totalmoney').html('Tổng tiền: ' + (response.totalmoney).toLocaleString('en-US') + ' VNĐ');
                        }else{
                            $('#num_' + productId).val(response.quantity);
                            $('#total_' + productId).html((response.total).toLocaleString('en-US'));
                            $('#totalmoney').html('Tổng tiền: ' + (response.totalmoney).toLocaleString('en-US') + ' VNĐ');
                        }
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }

        function call(){
            alert('Vui lòng đăng nhập để đặt hàng');
        }
   </script>
</body>
</html>